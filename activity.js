let student1 = {
	name: "Shawn Michaels",
	birthday: "May 5, 2003",
	age: 18,
	isEnrolled: true,
	classes: ["Philosphy 101","Social Sciences 201"]
}

const {name,birthday,age,isEnrolled,classes} = student1;

let student2 = {
	name2: "Steve Austin",
	birthday2: "June 15, 2001",
	age2: 20,
	isEnrolled2: true,
	classes2: ["Philosphy 401", "Natural Sciences 402"]
}

const {name2,birthday2,age2,isEnrolled2,classes2} = student2; 

function introduce(student){

	console.log(`Hi! I'm ${name}. I am ${age} years old.`);
	console.log(`I study the following courses ${classes}.`);
}



const getCube = (num) => num ** 3;


let cube = getCube(3);

console.log(cube);


let numArr = [15,16,32,21,21,2];

const [no1,no2,no3,no4,no5,no6] = numArr;

numArr.forEach(num => console.log(num));

let numsSquared = numArr.map(num => num ** 2);

console.log(numsSquared);


class Dog{
	constructor(name,breed,dogAge){

		this.name = name;
		this.breed = breed;
		this.dogAge = dogAge * 7;

	}
}

let dog1 = new Dog("Browny","Labrador",3);
let dog2 = new Dog("Sheppy","German Sheperd",2);
console.log(dog1);
console.log(dog2);
